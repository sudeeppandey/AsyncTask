package pandey.sudeep.asynctask;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;


public class TaskFragment extends Fragment implements TriggerTask {

    private static final String TAG = TaskFragment.class.getSimpleName();

    interface TaskCallbacks {
        void onPreExecute();
        void onProgressUpdate(Integer prog);
        //void onCancelled();
        void onPostExecute(Integer myInt);
    }

    private TaskCallbacks callbacks;
    private TestTask task;

    @Override
    public void onAttach(Context context){
        super.onAttach(context);
        Log.d(TAG, "onAttach() - Fragment attached with activity..............");
        callbacks = (TaskCallbacks)context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        Log.d(TAG, "onCreate() - Fragment initializing..............");
    }

    @Override
    public void onDetach(){
        super.onDetach();
        callbacks=null;
        Log.d(TAG, "onDetach() - Fragment detached from activity..............");
    }

    @Override
    public void startAsync(){
        new TestTask().execute();
    }

    private class TestTask extends AsyncTask<Void,Integer,Integer>{

        private int update=0;

        @Override
        protected void onPreExecute(){
            if(callbacks!=null){
                callbacks.onPreExecute();
            }
            Log.d(TAG, "onPreExecute() - Fragment's Async pre-executed..............");
        }

        @Override
        protected Integer doInBackground(Void... ignore){
            for(int i=0;i<10;i++){
                Log.d(TAG, "doInBackground() - Fragment's background task running..................");
                try {
                    Thread.sleep(3000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                update=update+1;
                publishProgress(update);
            }
            return update;
        }

        @Override
        protected void onProgressUpdate(Integer... myInts){
            if(callbacks!=null){
                callbacks.onProgressUpdate(myInts[0]);
                Log.d(TAG, "onProgressUpdate() - Fragment updating progress..............");
            }
        }
        @Override
        protected void onPostExecute(Integer result){
            if(callbacks!=null){
                callbacks.onPostExecute(result);
                Log.d(TAG, "onPostExecute() - Fragment's post-execution accomplished...................");
            }
        }
    }
}
