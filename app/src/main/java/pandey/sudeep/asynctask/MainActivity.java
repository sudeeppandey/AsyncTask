package pandey.sudeep.asynctask;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

interface TriggerTask{
    void startAsync();
}

public class MainActivity extends AppCompatActivity implements TaskFragment.TaskCallbacks {

    private static final String TAG = MainActivity.class.getSimpleName();

    private TaskFragment mTaskFragment;

    private static final String TAG_TASK_FRAGMENT = "task_fragment";

    private TriggerTask triggerTask;

    private TextView preEx;
    private TextView doBack;
    private TextView progress;
    private TextView postEx;

    private Button button;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        preEx = findViewById(R.id.textView);
        doBack = findViewById(R.id.textView2);
        progress = findViewById(R.id.textView3);
        postEx = findViewById(R.id.textView4);
        button = findViewById(R.id.button);

        android.support.v4.app.FragmentManager fm = getSupportFragmentManager();
        mTaskFragment = (TaskFragment) fm.findFragmentByTag(TAG_TASK_FRAGMENT);

        // If the Fragment is non-null, then it is currently being
        // retained across a configuration change.
        if (mTaskFragment == null) {
            mTaskFragment = new TaskFragment();
            fm.beginTransaction().add(mTaskFragment, TAG_TASK_FRAGMENT).commit();
            Log.d(TAG, "commit() - Fragment Transaction committed.................");
        }

        triggerTask = (TriggerTask)mTaskFragment;

        Log.d(TAG, "onCreate() - Creating activity state..............");
        if(savedInstanceState!=null){
            Log.d(TAG, "onCreate() - Recreating activity state..............");
        }

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                triggerTask.startAsync();
                Log.d(TAG, "startAsync() - Starting async task via interfacing..............");
            }
        });
    }

    @Override
    public void onStart(){
        super.onStart();
        Log.d(TAG, "onStart() - Starting activity..............");
    }

    @Override
    public void onResume(){
        super.onResume();
        Log.d(TAG, "onResume() - Resuming activity..............");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d(TAG, "onPause() - Pausing activity..............");
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.d(TAG, "onStop() - Activity Stopped.....................");
    }

    @Override
    public void onRestart() {
        super.onRestart();
        Log.d(TAG, "onRestart() - Activity Restarted.....................");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy() - Activity Destroyed.....................");
    }

    @Override
    public void onPreExecute(){
        preEx.setText("just pre-Executed the AsyncTask");
        doBack.setText("background task will now begin");

    }

    @Override
    public void onProgressUpdate(Integer val){

        progress.setText(Integer.toString(val));
    }

    @Override
    public void onPostExecute(Integer myVal){

        postEx.setText(Integer.toString(myVal));
    }

}
